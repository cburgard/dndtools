def throwset():
    from random import randint
    abilities = []
    for i in range(0,6):
        d6 = [ randint(1,6) for j in range(0,4) ]
        abilities.append(sum(sorted(d6)[1:]))
    return sorted(abilities,reverse=True)
            

def testset(candidate,toy):
    for i in range(0,len(candidate)):
        if candidate[i] < toy[i]:
            return False
    return True

def main(args):
    scores = sorted(args.score,reverse=True)
    counter = 0
    for i in range(0,args.toys):
        toy = throwset()
        accept = testset(scores,toy)
        if accept: counter += 1
    print(counter / args.toys)


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="calculate p-value of ability scores")
    parser.add_argument("score",nargs=6,type=int)
    parser.add_argument("--toys",type=int,default=10000)
    main(parser.parse_args())
